<?php

/**
 * Implementation of hook_menu().
 */
function toc_menu() {
  $items = array();

  $items['admin/settings/toc'] = array(
    'title' => 'Table of contents',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('toc_settings_form'),
    'access arguments' => array('administer site configuration'),
  );

  return $items;
}

function toc_settings_form() {
  $form = array();

  $form['toc_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#description' => t('Allow table of content generation on content of the selected node types.'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('toc_node_types', array()),
  );

  $form['toc_start_level'] = array(
    '#type' => 'select',
    '#title' => t('Start level'),
    '#description' => t('Starting from which heading level you want to generate the table of contents.'),
    '#options' => array(1 => 'h1', 2 => 'h2', 3 => 'h3', 4 => 'h4', 5 => 'h5', 6 => 'h6', 8 => 'h7'),
    '#default_value' => variable_get('toc_start_level', 2),
  );

  $form['toc_depth'] = array(
    '#type' => 'select',
    '#title' => t('Depth'),
    '#description' => t('How deep from the start level do you want to generate the table of contents.'),
    '#options' => range(1, 6),
    '#default_value' => variable_get('toc_depth', 1),
  );

  $form['toc_scope'] = array(
    '#type' => 'textfield',
    '#title' => t('Scope'),
    '#description' => t('jQuery selector defining the scope of the table of contents.'),
    '#default_value' => variable_get('toc_scope', '.node .content:eq(0)'),
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_form_alter().
 */
function toc_form_alter(&$form, $form_state, $form_id) {
  if (isset($form['#node']) && $form['#node']->type .'_node_form' == $form_id) {
    if (!in_array($form['#node']->type, toc_allowed_node_types())) return;

    $form['toc'] = array(
      '#type' => 'fieldset',
      '#title' => t('Table of contents'),
      '#collapsible' => TRUE,
      '#collapsed' => !$form['#node']->toc,
    );
    $form['toc']['toc'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show table of contents.'),
      '#default_value' => $form['#node']->toc,
    );
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function toc_nodeapi(&$node, $op, $teaser, $page) {
  if (!in_array($node->type, toc_allowed_node_types())) return;

  switch ($op) {
    case 'insert':
    case 'update':
      if ($op == 'update') {
        db_query("DELETE FROM {toc} WHERE nid = %d", $node->nid);
      }
      if ($node->toc) {
        db_query("INSERT INTO {toc} (nid) VALUES (%d)", $node->nid);
      }
      break;
    case 'delete':
      db_query("DELETE FROM {toc} WHERE nid = %d", $node->nid);
      break;
    case 'load':
      $toc = db_result(db_query("SELECT nid FROM {toc} WHERE nid = %d", $node->nid));
      $node->toc = $toc ? TRUE : FALSE;
      break;
    case 'view':
      if ($page && $node->toc) {
        $settings = array(
          'toc_start_level' => variable_get('toc_start_level', 2),
          'toc_depth' => variable_get('toc_depth', 1),
          'toc_scope' => variable_get('toc_scope', '.node .content:eq(0)'),
        );
        drupal_add_js($settings, 'setting');
        drupal_add_js(drupal_get_path('module', 'toc') .'/js/jquery.tableofcontents.min.js');
        drupal_add_js(drupal_get_path('module', 'toc') .'/js/jquery.smoothanchors2.min.js');
        drupal_add_js(drupal_get_path('module', 'toc') .'/js/toc.js');
      }
      break;
  }
}

function toc_allowed_node_types() {
  return array_keys(array_filter(variable_get('toc_node_types', array())));
}