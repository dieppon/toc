if (Drupal.jsEnabled) {
  $(document).ready(function() {
    var toc = $('<ul id="toc"></ul>');
    toc = toc.tableOfContents($(Drupal.settings.toc_scope), { startLevel: Drupal.settings.toc_start_level, depth: Drupal.settings.toc_depth });
    if ($('li', toc).length > 0) {
      $(Drupal.settings.toc_scope).prepend(toc);
      $.smoothAnchors(400, "swing", false);
      $(toc).prepend('<div class="title">In this section</div>');
    }
  });
}